package com.prex.common.log.feign.fallback;

import com.prex.base.api.entity.SysLog;
import com.prex.common.core.utils.R;
import com.prex.common.log.feign.RemoteLogService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @Classname RemoteLogFallbackImpl
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-08-24 10:56
 * @Version 1.0
 */
@Slf4j
@AllArgsConstructor
public class RemoteLogFallbackImpl implements RemoteLogService {

    private final Throwable throwable;

    @Override
    public R saveLog(SysLog sysLog) {
        log.error("feign 保存日志失败:{}", sysLog, throwable);
        return null;
    }
}
